// exercise 1
const rateEl = document.getElementById("rate");
const dayEl = document.getElementById("day");
const salaryEl = document.getElementById("salary");

const calcSalary = () => {
  const rateVal = +rateEl.value;
  const dayVal = +dayEl.value;
  const salaryVal = dayVal * rateVal;
  salaryEl.innerHTML = "$" + salaryVal.toFixed(2);
  console.log(rateVal, dayVal);
  console.log(salaryVal);
};

// exercise 2
const firstNumEl = document.getElementById("firstNum");
const secondNumEl = document.getElementById("secondNum");
const thirdNumEl = document.getElementById("thirdNum");
const fourthNumEl = document.getElementById("fourthNum");
const fifthNumEl = document.getElementById("firstNum");
const avgEl = document.getElementById("avg");

const calcAvg = () => {
  const firstNumVal = +firstNumEl.value;
  const secondNumVal = +secondNumEl.value;
  const thirdNumVal = +thirdNumEl.value;
  const fourthNumVal = +fourthNumEl.value;
  const fifthNumVal = +fifthNumEl.value;

  const avgVal =
    (firstNumVal + secondNumVal + thirdNumVal + fourthNumVal + fifthNumVal) / 5;
  avgEl.innerHTML = avgVal.toFixed(2);
};

// exercise 3
const usdEl = document.getElementById("usd");

const vndConvertedEl = document.getElementById("vndConverted");

const convert = () => {
  const usd = +usdEl.value;
  const vndConverted = usd * 23500;

  vndConvertedEl.innerHTML = vndConverted;
};

// exercise 4
const lengthEl = document.getElementById("length");

const widthEl = document.getElementById("width");

const areaEl = document.getElementById("area");

const perimeterEl = document.getElementById("perimeter");

const calculate = () => {
  const length = +lengthEl.value;

  const width = +widthEl.value;

  const area = length * width;

  const perimeter = (length + width) * 2;

  areaEl.innerHTML = `Area: ${area} `;
  perimeterEl.innerHTML = `Perimeter: ${perimeter}`;
};

// exercise 5
const numberEl = document.getElementById("number");

const totalEl = document.getElementById("total");

const calc = () => {
  const number = +numberEl.value;

  const firstDigit = Math.round(number / 10);
  const secondDigit = number % 10;
  const total = firstDigit + secondDigit;

  totalEl.innerHTML = total;
};
